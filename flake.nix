{
  description = "Atlas data flake";
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-22.11;
    nixpkgs-22_05.url = github:NixOS/nixpkgs/nixos-22.05;
    # myvim.url = "/private/nixModule/neovim";
  };
  # outputs = { self, nixpkgs, nixpkgs-22_05, myvim }:
  outputs = { self, nixpkgs, nixpkgs-22_05 }:
    let
      project = "sfa-atlas-data";
      system = "x86_64-linux";
      overlays = [
        (self: super: {
          haskellPackages = super.haskellPackages.override {
            overrides = haskellSelf: haskellSuper: {
              geos = self.haskell.lib.dontCheck (
                self.haskell.lib.unmarkBroken super.haskellPackages.geos
              );
              # jose-jwt = self.haskell.lib.unmarkBroken super.haskellPackages.jose-jwt;
            };
          };
          geoBs = import ./geoBsLayers.nix {
            inherit pkgs;
            pgHost = pgSettings.tmp;
            dbName = bsGeoDbName;
            psql = pgSettings.psql;
          };
        })
      ];
      pkgs = import nixpkgs {
        inherit system;
        inherit overlays;
      };
      pkgs-22_05 = import nixpkgs-22_05 {
        inherit system;
      };
      haskellCmds = (pkgs.haskellPackages.callCabal2nix "sfa-data" ./. {});
      bsGeoDbName = "bsgeo";   # data from GeoBS and OSM
      hp = pkgs.haskellPackages;
      sfaLegacyDbName = "sfa-legacy";         # our Sfa postgres 10 data
      pgSettings = {
        psql =  pkgs.postgresql.withPackages (p: [ p.postgis ]);
        tmp  = "/tmp/postgres/";
        data = "postgres_data";
      };
      pg10Settings = {
        psql =  pkgs-22_05.postgresql_10.withPackages (p: [ p.postgis ]);
        tmp  = "/tmp/postgres10/";
        data = "postgres10_data";
      };
      defaultPackage.${system} = haskellCmds;
      projectShell = hp.shellFor {
        packages = p: [ haskellCmds ];
        buildInputs = with pkgs; [
          # hp.apply-refact hp.hlint hp.stylish-haskell hp.hasktags hp.hoogle
          hp.cabal-install
          gdb
          git
          hp.haskell-debug-adapter
          hp.haskell-language-server
          hp.hoogle
          gdal
          git
          osm2pgsql
          osmium-tool
          pgSettings.psql
          pkgs.geoBs
          (writeShellScriptBin "copyWohnviertelFromGeoBsToSfaDataDb" ''
           pg_dump -h ${pgSettings.tmp} -U postgres -t wohnviertel bsgeo | psql -h ${pgSettings.tmp} -U $DBUSER $DBNAME
           '')
          (writeShellScriptBin "dumpLocalSfaData" ''
           pg_dump -U $DBUSER -h /tmp/postgres $DBNAME > "dbDump/localDump_$(date +%Y-%m-%d).sql"
           '')
          (writeShellScriptBin "pgStart" ''
           ${pgSettings.psql}/bin/pg_ctl start -D ${pgSettings.data} -l ${pgSettings.tmp + "LOG"} \
           -o "-c listen_addresses= -c unix_socket_directories=${pgSettings.tmp}"
           '')
          (writeShellScriptBin "pg10Stop" ''
           ${pg10Settings.psql}/bin/pg_ctl stop -D ${pg10Settings.data} \
           -o "-c listen_addresses='*' -c unix_socket_directories=${pg10Settings.tmp}"
           '')
          (writeShellScriptBin "pg10Start" ''
           ${pg10Settings.psql}/bin/pg_ctl start -D ${pg10Settings.data} -l ${pg10Settings.tmp + "LOG"} \
           -o "-c listen_addresses='*' -c unix_socket_directories=${pg10Settings.tmp}"
           '')
          (writeShellScriptBin "initSfaLegacyDb" ''
           ${pg10Settings.psql}/bin/createdb -h ${pg10Settings.tmp} -U $LEGACY_DBUSER ${sfaLegacyDbName}
           ${pg10Settings.psql}/bin/psql -h ${pg10Settings.tmp} -U $LEGACY_DBUSER ${sfaLegacyDbName} -c "CREATE EXTENSION postgis;"
           ${pg10Settings.psql}/bin/psql -h ${pg10Settings.tmp} -U $LEGACY_DBUSER ${sfaLegacyDbName} -c "CREATE ROLE mapper1 LOGIN CREATEDB;"
           ${pg10Settings.psql}/bin/psql -h ${pg10Settings.tmp} -U $LEGACY_DBUSER ${sfaLegacyDbName} -c "CREATE ROLE rapper1 LOGIN CREATEDB;"
           ${pg10Settings.psql}/bin/psql -h ${pg10Settings.tmp} -U $LEGACY_DBUSER ${sfaLegacyDbName} < $1
           '')
          (writeShellScriptBin "initSfaAtlasDb" ''
           echo "init sfa altas db"
           ${pgSettings.psql}/bin/createdb -h ${pgSettings.tmp} -U $DBUSER $DBNAME
           ${pgSettings.psql}/bin/psql -h ${pgSettings.tmp} -U $DBUSER $DBNAME -c "CREATE EXTENSION postgis;"
           '')
          (writeShellScriptBin "initBsGeoDb" ''
           echo "init rawgeo db"
           ${pgSettings.psql}/bin/createdb -h ${pgSettings.tmp} -U $DBUSER ${bsGeoDbName}
           ${pgSettings.psql}/bin/psql -h ${pgSettings.tmp} -U $DBUSER ${bsGeoDbName} -c "CREATE EXTENSION postgis;"
           echo "rawgeo db: init geobs"
           '')
          (writeShellScriptBin "restoreSfaAtlasDbToIfog" ''
           psql -U $PUBLIC_SFA_DBUSER -d $DBNAME -h  $PUBLIC_HOST_IP -p $PUBLIC_SFA_DBPORT -f $1
           '')
          (writeShellScriptBin "dumpSfaDb" ''
           mkdir -p dbDump
           pg_dump -U $PUBLIC_LEGACY_DBUSER -h $PUBLIC_HOST_IP -p $PUBLIC_LEGACY_DBPORT $PUBLIC_LEGACY_DBNAME > "dbDump/legacyDump_$(date +%Y-%m-%d).sql"
           '')
          ];
        withHoogle = true;
        shellHook =
          ''
          export PS1='✊\u@${project} \$ '
          source env.sh
          if [ ! -d ${pg10Settings.tmp} ]; then
            mkdir -p ${pg10Settings.tmp}
        fi
          if [ ! -d ${pgSettings.tmp} ]; then
            mkdir -p ${pgSettings.tmp}
        fi

          if [ ! -d ${pg10Settings.data} ]; then
            echo 'Initializing postgresql 10 database...'
              ${pg10Settings.psql}/bin/initdb ${pg10Settings.data} -U $LEGACY_DBUSER --auth=trust --locale en_US.UTF-8 # >/dev/null
              echo 'Starting postgresql 10 database...'
              pg10Start
              fi
              if [ ! -d ${pgSettings.data} ]; then
                echo 'Initializing postgresql database...'
                  ${pgSettings.psql}/bin/initdb ${pgSettings.data} -U $DBUSER --auth=trust --locale en_US.UTF-8 >/dev/null
                  pgStart
                  initSfaAtlasDb
                  initBsGeoDb
                  fi
                  echo "Checking postgres status..."
                  ${pg10Settings.psql}/bin/pg_ctl -D ${pg10Settings.data} status
                  if [ $? -eq 3 ]; then
                    pg10Start
                      fi
                      ${pgSettings.psql}/bin/pg_ctl -D ${pgSettings.data} status
                      if [ $? -eq 3 ]; then
                        pgStart
                          fi
                          '';
      };
    in rec {
      packages.${system} = {
        bsGeo = pkgs.geoBs;
      };
      devShell.${system} = pkgs.mkShell {
        packages = [
          # myvim.packages.x86_64-linux.neovim-haskell
        ];
        inputsFrom = [
          projectShell
          # myvim.devShells.x86_64-linux.neovim-haskell
        ];
      };
    };
}
