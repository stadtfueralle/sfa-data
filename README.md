![SFA Logo](https://map.stadtfueralle.info/static/img/logo.png "Stadt für Alle"){width=100px height=100px}

# Data for SfA Atlas

## enter the nix development shell

Run `nix develop` to build and enter the nix development shell.

## nix development environment

The nix shell reads the needed environment from a filed called env.sh.
Create this file with following values.

``` bash
export ENVIRONMENT=development
export APIHOST=http://localhost:8000
export PUBLIC_HOST_IP=
export PUBLIC_LEGACY_DBPORT=
export PUBLIC_LEGACY_DBUSER=
export PUBLIC_LEGACY_DBNAME=
export PUBLIC_SFA_DBPORT=
export PUBLIC_SFA_DBUSER=
export DBHOST=/tmp/postgres
export DBPORT=5432
export DBUSER=postgres
export DBPASS="password of local db instance"
export DBNAME=sfa-data
export LEGACY_DBHOST=/tmp/postgres10
export LEGACY_DBPORT=5432
export LEGACY_DBUSER=postgres
export LEGACY_DBNAME=sfa-legacy
export LEGACY_DBPASS="psw of local legacy db instance"
```

## import geo layers from Basel-Stadt

Search and find following data sets on [Geoportal Basel-Stadt](https://shop.geo.bs.ch/).
  - DM01AVBS06D
  - DM_Datenmarkt
  - WE_StatistischeRaumeinheiten

In the download dialog, choose the `Esri Shapefile` option. A link to a
downloadable zip file will be sent to your email. Save the zip file in the folder
`./bsGeoLayers`.

Now you are ready to import those Shapefiles to the local db instance with
following commands:

``` bash
initBsGeoDb       # defined in flake.nix
importBsGeoLayers # defined in geoBsLayers.nix
```

The command `importBsGeoLayers` looks up `./bsGeoLayers` for zip files called
after the nick for each layer noted in `./geoBsLayers.nix`, e.g.
`DM_Datenmarkt.zip`. It unzips the Esri shape files to `./unzipped` and imports
them to the postgres db.

## import legacy sfa data

You need a sql dump file of the old legacy sfa database. At the end of its life,
it was running on postgres version 10. So we need a local postgres 10 instance
running. Once you have got the dump file, you can import it to the local
postgres instance using the command

``` bash
initSfaLegacyDb ${name of the dumped sql file}
```

## convert legacy db to new db layout

To build the tools for legacy migration build the nix project:
``` bash
nix build '.?submodules=1#'.
```

Then migrate a fresh sfa db and import the legacy db to it:
``` bash
./result/bin/migrate-db
./result/bin/import-legacy-to-sfa
```

The ladder command will combine the legacy data from our postgres 10 instance
with the bsGeo data from our local up-to-date postgres instance and create
entries in our freshly migrated sfa-data database.

## Git submodules

Add submodule 

``` bash
git submodule add https://codeberg.org/stadtfueralle/sfa-atlas
```

Remove submodule

``` bash
git rm <path-to-submodule>
rm -rf .git/modules/<path-to-submodule>
git config --remove-section submodule.<path-to-submodule>
```
