{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module DbRawGeo ( connString
                , qBaurecht
                , qParzelle
                , BsGeoParcel (..)
                , BsGeoGebaeude (..)
                , BsGeoEingang (..)
                , qGebaeudeGeoBs
                , qEingangByEgid
                ) where

import           Data.ByteString.Char8                (pack)
import           Data.Geometry.Geos.Geometry          (Geometry (MultiPolygonGeometry, PointGeometry, PolygonGeometry),
                                                       MultiPolygon (..),
                                                       Point (..), Polygon (..),
                                                       Some (Some))
import           Data.Geometry.Geos.Serialize         (readHex)
import           Data.String.Here                     (iTrim)
import qualified Data.Text                            as T
import           Database.PostgreSQL.Simple
import           Database.PostgreSQL.Simple.FromField
import           Database.PostgreSQL.Simple.FromRow
import           System.Environment


import           PostgisHaskellBinding                (Geo (..))

-- ! Instances for postgresql-simple
instance FromField (Geo Point) where
        fromField _ m = case m of
              Just bs -> case readHex bs of
                Just (Some (PointGeometry mpl (Just 4326))) ->
                  return $ GeoPoint mpl
                x -> return $ error ("eRrOr Point: " ++ show x)
              Nothing -> error "Invalid Field"

instance FromField (Geo Polygon) where
        fromField _ m = case m of
              Just bs -> case readHex bs of
                Just (Some (PolygonGeometry mpl (Just 4326))) ->
                  return $ GeoPolygon mpl
                x -> return $ error ("eRrOr Polygon" ++ show x)
              Nothing -> error "Invalid Field"

instance FromField (Geo MultiPolygon) where
        fromField _ m = case m of
              Just bs -> case readHex bs of
                Just (Some (MultiPolygonGeometry mpl (Just 4326))) ->
                  return $ GeoMultiPolygon mpl
                x -> return $ error ("eRrOr MultiPolygon" ++ show x)
              Nothing -> error "Invalid Field"

instance FromField (Some Geo) where
        fromField _ m = case m of
              Just bs -> case readHex bs of
                Just (Some (PointGeometry p (Just 4326))) ->
                    return $ Some $ GeoPoint p
                Just (Some (PolygonGeometry mpl (Just 4326))) ->
                    return $ Some $ GeoPolygon mpl
                Just (Some (MultiPolygonGeometry mpl (Just 4326))) ->
                    return $ Some $ GeoMultiPolygon mpl
                _ -> return $ error "Invalid Some Geo Geography hex"
              Nothing -> error "Invalid Some Geo Field"
              -- Just bs -> return $ readGeometry $ BSL.fromStrict bs


data BsGeoEingang = BsGeoEingang
  { eingangGeom :: Geo Point
  , eingangEgid :: Double
  , eingangEdid :: Maybe Int
  , eingangNr   :: T.Text
  } deriving (Show)

data BsGeoGebaeude = BsGeoGebaeude
  { bsGeoEgid         :: Double
  , bsGeoStatus       :: T.Text
  , bsGeoGebaeudeGeom :: Geo MultiPolygon
  } deriving (Show)

data BsGeoParcel = BsGeoParcel
  { parcelGeom               :: Geo MultiPolygon
  , parcelEgrid              :: T.Text
  , parcelArea               :: Double
  , parcelGrundbuchkreisnumm :: T.Text
  , parcelGrundstuecksnummer :: T.Text
  , parcelArt                :: T.Text
  } deriving (Show)

instance FromRow BsGeoGebaeude where
  fromRow = BsGeoGebaeude <$> field <*> field <*> field

instance FromRow BsGeoEingang where
  fromRow = BsGeoEingang <$> field <*> field <*> field <*> field

instance FromRow BsGeoParcel where
  fromRow = BsGeoParcel <$> field <*> field <*> field <*> field <*> field <*> field

-- access to fresh postgres 11 db, several dbs are running here
connString :: IO String
connString = do
  host <- getEnv "DBHOST"
  port <- getEnv "DBPORT"
  user <- getEnv "DBUSER"
  pass <- getEnv "DBPASS"
  return $ "host="++ host
       ++ " port=" ++ port
       ++ " user=" ++ user
       ++ " dbname=" ++ "bsgeo"
       ++ " password=" ++ pass

simpleQuery :: (FromRow a, ToRow q) => Query -> q -> IO [a]
simpleQuery queryString args = do
  conn <- connectPostgreSQL . pack =<< connString
  rows <- query conn queryString args
  close conn
  return rows

qGebaeudeGeoBs :: String -> IO [BsGeoGebaeude]
qGebaeudeGeoBs boundary = simpleQuery q [boundary | boundary /= "Basel"]
  where
    q :: Query
    q = case boundary of
      "Basel" ->
         [iTrim| SELECT gebnrbfs, gebstattxt, geom
                 FROM datenmarktgebaeudeflaeche;
               |]
      _       ->
         [iTrim| SELECT gebnrbfs, gebstattxt, geom
                 FROM datenmarktgebaeudeflaeche
                 WHERE ST_Intersects(geom,(
                   SELECT geom
                   FROM wohnviertel
                   WHERE wov_name = ?
                 ));
              |]

qEingangByEgid :: Double -> IO [BsGeoEingang]
qEingangByEgid egid = simpleQuery q [egid]
  where
    q :: Query
    q = [iTrim|
              SELECT geom, gwr_egid, gwr_edid, hausnummer
              FROM ge_gebaeudeeingang
              WHERE gwr_egid = ?;
              |]

qBaurecht :: String -> IO [BsGeoParcel]
qBaurecht boundary = simpleQuery q [boundary | boundary /= "Basel"]
  where
    q :: Query
    q = case boundary of
      "Basel" ->
        [iTrim|
              SELECT geom, r1_egris_e, flaechenma, r1_sektion, r1_nummer, r1_art_txt
              FROM li_selbstrecht
              |]
      _ ->
        [iTrim|
              SELECT geom, r1_egris_e, flaechenma, r1_sektion, r1_nummer, r1_art_txt
              FROM li_selbstrecht
              WHERE ST_Intersects(geom,(
                SELECT geom
                FROM wohnviertel
                WHERE wov_name = ?
              ))
              |]

qParzelle :: String -> IO [BsGeoParcel]
qParzelle boundary = do
  simpleQuery q [boundary | boundary /= "Basel"]
  where
    q :: Query
    q = case boundary of
      "Basel" ->
        [iTrim|
              SELECT geom, r1_egris_e, flaechenma, r1_sektion, r1_nummer, r1_art_txt
              FROM li_liegenschaft;
              |]
      _ ->
        [iTrim|
              SELECT geom, r1_egris_e, flaechenma, r1_sektion, r1_nummer, r1_art_txt
              FROM li_liegenschaft
              WHERE ST_Intersects(geom,(
                SELECT geom
                FROM wohnviertel
                WHERE wov_name = ?
              ));
              |]
