{-# LANGUAGE OverloadedStrings #-}

module DbImportBsGeo ( popGebaeudeByQuartier
                     , popParzelleByQuartier
                     , handleNoEingang
                     ) where

import           Data.Int                             (Int64)
import qualified Data.Text                            as T
import           Data.Time                            (UTCTime, getCurrentTime)
import           Database.Persist.Class.PersistEntity (Entity (..))
import           Database.Persist.Sql                 (Key, fromSqlKey, getBy,
                                                       insertUnique, selectList,
                                                       update, (=.), (==.))
import           GHC.Float                            (double2Int)

import qualified Database                             as Db
import qualified DatabaseGebaeude                     as DbG
import qualified DbRawGeo                             as RawGeo
import qualified QueryAdminCh                         as ACh
import qualified SchemaEingang                        as SchE
import qualified SchemaGebaeude                       as SchG
import qualified SchemaParzelle                       as SchP

-- | Populate osm data gebaeude and eingang based on AdminChEingang data
-- currently present in rawGeo db. AdminChEingang's populate function is defined
-- in RawGeo.hs
popGebaeudeByQuartier :: String -> IO ()
popGebaeudeByQuartier quartier = do
  gList <- RawGeo.qGebaeudeGeoBs quartier
  mapM_ handleRawGebaeude gList

-- | First we create a gebaeude we the info we alreadey have vom the bs geo
-- shop. Then we check two sources for info about a Gebaeude and its Eingaenge. Most
-- info we get from the admin ch api. If we find an entry there, we will get more
-- info about the gebaeude itself and a list of its eingaenge. Some are not
-- found there, so we check if we find eingaenge in our rawGeo
-- database. If not found there either, no eingang will be created,
handleRawGebaeude :: RawGeo.BsGeoGebaeude -> IO ()
handleRawGebaeude rawG = do
  geb <- createRawGebaeude rawG
  adminChL <- ACh.queryByEgid $ RawGeo.bsGeoEgid rawG
  case adminChL of
    l@(x:_) -> createAdminChGebaeude geb x
            >> mapM_ (createAdminChEingang $ RawGeo.bsGeoEgid rawG) l
    _       -> print $ "No Eingang found for Gebaeude with egid "
            ++ (show . double2Int) (RawGeo.bsGeoEgid rawG)

-- | actually there should be an Eingang for each Gebaeude. If there is a
-- Gebaeude without Eingang, this function can be used to recheck admin ch and
-- update the Gebaeude/create the found Eingaenge
handleNoEingang :: IO ()
handleNoEingang = do
  gebWoEingang <- DbG.qGebaeudeWithoutEingang
  print $ "found " ++ show (length gebWoEingang) ++ " Gebaeude wo Eingang"
  mapM_ recheckNoEingang gebWoEingang

recheckNoEingang :: Entity SchG.Gebaeude -> IO ()
recheckNoEingang (Entity key geb) = do
  let egid = SchG.gebaeudeEgid geb
  adminChL <- ACh.queryByEgid egid
  case adminChL of
    l@(x:_) -> updateAdminChGebaeude key x
            >> mapM_ (createAdminChEingang $ SchG.gebaeudeEgid geb) l
    _       -> print $ "No Eingang found for Gebaeude with egid "
                     ++ show (double2Int egid)

createRawGebaeude :: RawGeo.BsGeoGebaeude -> IO SchG.Gebaeude
createRawGebaeude rawG = SchG.Gebaeude (RawGeo.bsGeoGebaeudeGeom rawG)
                                       (RawGeo.bsGeoEgid rawG)
                                       (RawGeo.bsGeoStatus rawG)
                                       ""
                                       Nothing
                                       Nothing
                                       Nothing
                                       Nothing
                                       Nothing
                                       Nothing
                                       Nothing
                                       Nothing
                                       <$> getCurrentTime

updateAdminChGebaeude :: Key SchG.Gebaeude -> ACh.AdminChEingang -> IO ()
updateAdminChGebaeude key e =
  let
    updateG = [ SchG.GebaeudeEgrid           =. ACh.egrid e
              , SchG.GebaeudeBaujahr         =. ACh.baujahr e
              , SchG.GebaeudeAbbruchjahr     =. ACh.abbruchjahr e
              , SchG.GebaeudeFlaeche         =. ACh.gebaeudeflaeche e
              , SchG.GebaeudeBezeich         =. ACh.gebbezeichnung e
              , SchG.GebaeudeVolumen         =. ACh.gebaeudevolumen e
              , SchG.GebaeudeKategor         =. Just (ACh.gebaeudekategor e)
              , SchG.GebaeudeAnzahlgeschosse =. ACh.anzahlstockwerk e
              , SchG.GebaeudeAnzahlwohnung   =. ACh.anzahlwohnung e
              ]
  in do
    Db.runActionR (update key updateG)
    pure ()

createAdminChGebaeude :: SchG.Gebaeude -> ACh.AdminChEingang -> IO ()
createAdminChGebaeude rawG e = do
  parz <- Db.peelValues $ Db.qGeoAtPoint' "parzelle" (ACh.geom e)
  let
    egrid = case filter (\p -> "Liegenschaft" == SchP.parzelleTyp p) parz of
            (x:_) -> SchP.parzelleEgrid x
            []    -> ""
    geb = rawG { SchG.gebaeudeEgrid           = if T.null (ACh.egrid e)
                                                then egrid else ACh.egrid e
               , SchG.gebaeudeBaujahr         = ACh.baujahr e
               , SchG.gebaeudeAbbruchjahr     = ACh.abbruchjahr e
               , SchG.gebaeudeFlaeche         = ACh.gebaeudeflaeche e
               , SchG.gebaeudeBezeich         = ACh.gebbezeichnung e
               , SchG.gebaeudeVolumen         = ACh.gebaeudevolumen e
               , SchG.gebaeudeKategor         = Just $ ACh.gebaeudekategor e
               , SchG.gebaeudeAnzahlgeschosse = ACh.anzahlstockwerk e
               , SchG.gebaeudeAnzahlwohnung   = ACh.anzahlwohnung e
               }
  mbKey <- insertGebaeude geb
  case mbKey of
    Just _   -> pure ()
    Nothing  -> print $ "Skipping Gebaeude with EGID "
                     ++ show (double2Int $ SchG.gebaeudeEgid rawG)

createAdminChEingang :: Double -> ACh.AdminChEingang -> IO ()
createAdminChEingang egid eingang = do
  time <- getCurrentTime
  mbKey <- insertEingang $ SchE.Eingang (ACh.geom eingang)
                                        (Just $ ACh.egaid eingang)
                                        egid
                                        (case reads $ ACh.edid eingang of
                                           x:_ -> fst x
                                           _   -> 0
                                           )
                                        (ACh.strasse eingang)
                                        (ACh.nummer eingang)
                                        time
  case mbKey of
    Just _   -> pure ()
    Nothing  -> print $ "Skipping Eingang with Egaid "
                     ++ show (ACh.egaid eingang)

-- | Populate Parzelle based on Baurecht and Liegenschaft layers in Geo BS data.
popParzelleByQuartier :: String -> IO ()
popParzelleByQuartier quartier = do
  rawParzelle <- RawGeo.qParzelle quartier
  rawBaurecht <- RawGeo.qBaurecht quartier
  now <- getCurrentTime
  print now
  parzellen <- mapM insertParzelle (createParcel "Liegenschaft" now <$> rawParzelle)
  print $ show (filter (> 0) parzellen) ++ " Parzellen gefunden"
  baurechte <- mapM insertParzelle (createParcel "Baurecht" now <$> rawBaurecht)
  print $ show (filter (> 0) baurechte) ++ " Baurechte gefunden"
  where
    createParcel :: T.Text -> UTCTime -> RawGeo.BsGeoParcel -> SchP.Parzelle
    createParcel typ time p = SchP.Parzelle (RawGeo.parcelGeom p)
                                            (RawGeo.parcelEgrid p)
                                            typ
                                            (T.takeWhileEnd (/= '.') $ RawGeo.parcelArt p)
                                            (RawGeo.parcelArea p)
                                            (RawGeo.parcelGrundbuchkreisnumm p)
                                            (RawGeo.parcelGrundstuecksnummer p)
                                            time

insertGebaeude :: SchG.Gebaeude -> IO (Maybe (Key SchG.Gebaeude))
insertGebaeude entity = Db.runActionR (insertUnique entity)

insertEingang :: SchE.Eingang -> IO (Maybe (Key SchE.Eingang))
insertEingang entity = Db.runActionR (insertUnique entity)

insertParzelle :: SchP.Parzelle -> IO Int64
insertParzelle p = do
  newKeyMaybe <- Db.runActionR $ insertUnique p
  case newKeyMaybe of
    Just x  -> return $ fromSqlKey x
    Nothing -> print ("skipping Parzelle " ++ show (SchP.parzelleEgrid p)) >> return 0

qGebaeudeByEgid :: Double -> IO (Maybe (Entity SchG.Gebaeude))
qGebaeudeByEgid egid = do
  Db.runActionR (getBy (SchG.UniqueEgid egid))

-- | Not sure if an Eingang is spatially always inside its Gebaeude polygon.
-- Maybe a portal could also be outside of a gebaeude polygon? To make sure, I
-- added the attribute gebaeudeEgid to the Eingang schema
qEingangByGebaeude :: SchG.Gebaeude -> IO [Entity SchE.Eingang]
qEingangByGebaeude g = Db.runActionR $
  selectList [SchE.EingangGebaeudeEgid ==. SchG.gebaeudeEgid g] []
