module DbMigrate (migrate) where

import           Database.Persist.Sql           (entityDef)
import qualified Database.Persist.Sql.Migration as Mig

import qualified Database                       as Db
import qualified SchemaEingang                  as SchE
import qualified SchemaGebaeude                 as SchG
import qualified SchemaParzelle                 as SchP
import qualified SchemaUser                     as SchU

migrate :: IO ()
migrate = do
  Db.runActionR $ Mig.runMigration $ Mig.migrate SchE.entityDefs $
    entityDef (Nothing :: Maybe SchE.Eingang)
  Db.runActionR $ Mig.runMigration SchG.migrateAll
  Db.runActionR $ Mig.runMigration SchP.migrateAll
  Db.runActionR $ Mig.runMigration $ Mig.migrate SchU.entityDefs $
    entityDef (Nothing :: Maybe SchU.User)
