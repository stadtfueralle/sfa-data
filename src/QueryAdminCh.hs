{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DerivingStrategies    #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}


{-|
Fetch and parse data from api.geo.admin.ch and create a csv file
-}
module QueryAdminCh ( AdminChEingang (..)
                    , queryByEgid
                    ) where

import           Data.Aeson
import qualified Data.Aeson.KeyMap           as AKM
import           Data.Aeson.Types
import qualified Data.ByteString.Lazy        as BL
import qualified Data.ByteString.Lazy.Char8  as BL8
import           Data.Geometry.Geos.Geometry (Point, coordinate2, point)
import qualified Data.Text                   as T
import           GHC.Float                   (double2Int)
import           GHC.Generics                (Generic)
import           Network.HTTP.Base           (urlEncodeVars)
import           Network.HTTP.Simple         (getResponseBody, httpJSONEither,
                                              parseRequest)
import           PostgisHaskellBinding       (Geo (GeoPoint))
import           System.IO

data AdminChEingang = AdminChEingang
  { geom            :: Geo Point
  , egid            :: T.Text
  , edid            :: String
  , egrid           :: T.Text
  , egaid           :: Int
  , baujahr         :: Maybe Int
  , abbruchjahr     :: Maybe Int
  , gebaeudeflaeche :: Maybe Int
  , gebaeudevolumen :: Maybe Int
  , gebaeudekategor :: T.Text
  , gebaeudeklasse  :: Maybe Int
  , gebbezeichnung  :: Maybe T.Text
  , grundstuecknumr :: T.Text
  , grundbchkreisnr :: Int
  , anzahlstockwerk :: Maybe Int
  , anzahlwohnung   :: Maybe Int
  , strasse         :: T.Text
  , nummer          :: T.Text
  } deriving (Show, Generic)

-- | map json fields to general real estate fields
instance FromJSON AdminChEingang where
    parseJSON = withObject "AdminChEingang" $ \o -> do
        attributes      <- o          .:  "attributes"
        geometry        <- o          .:  "geometry"
        x               <- geometry   .:  "x"
        y               <- geometry   .:  "y"
        let geom =  GeoPoint $ point $ coordinate2 x y
        egid            <- attributes .:  "egid"
        edid            <- attributes .:  "edid"
        egrid           <- attributes .:  "egrid"
        egaid           <- attributes .:  "egaid"
        baujahr         <- attributes .:  "gbauj"
        abbruchjahr     <- attributes .:? "gabbj"
        gebaeudekategor <- decodeGebaeudeKat <$> attributes .: "gkat"
        gebaeudeklasse  <- attributes .: "gklas"
        gebaeudeflaeche <- attributes .:  "garea"
        gebaeudevolumen <- attributes .:  "gvol"
        gebbezeichnung  <- attributes .:? "gbez"
        grundstuecknumr <- attributes .:  "lparz"
        grundbchkreisnr <- attributes .:  "lgbkr"
        anzahlwohnung   <- attributes .:? "ganzwhg"
        anzahlstockwerk <- attributes .:? "gastw"
        strasse         <- head <$> attributes .: "strname"
        nummer          <- attributes .: "deinr"
        return (AdminChEingang{..})

decodeGebaeudeKat :: Int -> T.Text
decodeGebaeudeKat g = case g of
  1010 -> "prov. Unterkunft"
  1020 -> "ausschliesslich Wohnnutzung"
  1021 -> "reines Wohngebäude (Einfamillienhaus)"
  1025 -> "reines Wohngebäude (Mehrfamillienhaus)"
  1030 -> "Wohngebäude mit Nebennutzung"
  1040 -> "mit teilweiser Wohnnutzung"
  1060 -> "ohne Wohnnutzung"
  _    -> T.pack $ show  g
  -- 1110 -> "mit einer Wohnung"
  -- 1121 -> "mit zwei Wohnungen"
  -- 1122 -> "drei oder mehr Wohnungen"
  -- 1130 -> "Wohngebäude für Gemeinschaften"
  -- 1211 -> "Hotelgebäude"

-- | Parse response of polygon area query.
parsePgResults :: Value -> Parser [AdminChEingang]
parsePgResults = withObject "eingangList" $ \o ->
  case AKM.lookup "results" o of
    Just r  -> parseJSON r
    Nothing -> fail "no field 'results' :("

writeError :: String -> IO ()
writeError responseErr = do
  handle <- openFile "errors.txt" AppendMode
  putStrLn $ "writing to file...\n" ++ responseErr
  hPutStrLn handle responseErr
  hClose handle

queryByEgid :: Double -> IO [AdminChEingang]
queryByEgid egid = do
  -- BS.putStrLn [i|getting eingang ${double2Int egid}|]
  req <- (parseRequest . url . show . double2Int) egid
  response <- httpJSONEither req
  case getResponseBody response of
    Right a -> case parseEither parsePgResults a of
      Right eingangList -> return eingangList
      Left err          -> writeError (show err) >> return []
    Left err          -> writeError (show err) >> return []
  where url egid_ = "https://api3.geo.admin.ch/rest/services/api/MapServer/find?"
          ++ urlEncodeVars [ ("searchText", egid_)
                           , ("searchField","egid")
                           , ("sr","4326")
                           , ("layer", "ch.bfs.gebaeude_wohnungs_register")
                           , ("contains", "false") -- search exact pattern
                           ]

-- | Legacy function but nice example how to query features inside a polygon
queryByPg :: BL.ByteString -> IO [AdminChEingang]
queryByPg pg = do
  req <- parseRequest $ urlString pg
  response <- httpJSONEither req
  case getResponseBody response of
    Right a -> case parseEither parsePgResults a of
      Right eingangList -> return eingangList
      Left err          -> writeError (show err) >> return []
    Left err          -> writeError (show err) >> return []


-- | Take a Esri formatted polygon and form a geo admin url out of it.
urlString :: BL.ByteString -> String
urlString esriPolygon =
  "https://api3.geo.admin.ch/rest/services/all/MapServer/identify?"
  ++ urlEncodeVars [ ("geometry", BL8.unpack esriPolygon)
                   , ("geometryType", "esriGeometryPolygon")
                   , ("sr","4326")
                   , ("layers", "all:ch.bfs.gebaeude_wohnungs_register")
                   , ("tolerance", "0")
                   ]
