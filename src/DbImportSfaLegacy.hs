{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE TypeApplications  #-}

module DbImportSfaLegacy ( popInfo
                         , popEigentum
                         , delDoubleEntries
                         ) where

import           Control.Monad
import           Data.ByteString.Char8              (pack)
import           Data.Geometry.Geos.Geometry        (Geometry (PointGeometry),
                                                     Point, coordinate2, point)
import           Data.Geometry.Geos.Serialize       (writeHex)
import           Data.Int                           (Int64)
import           Data.Set                           (fromList, toList)
import           Data.Maybe                         as Mb
import           Data.String.Here                   (i, iTrim)
import qualified Data.Text                          as T
import           Data.Time                          (Day, UTCTime (..))
import qualified Database.Persist.Sql               as Sql
import           Database.PostgreSQL.Simple
import           Database.PostgreSQL.Simple.FromRow
import           System.Environment

import           Database.Esqueleto.Experimental

import qualified Database                           as Db
import qualified SchemaGebaeude                     as Gebaeude
import qualified SchemaParzelle                     as Parzelle
import qualified SchemaEingang                      as Eingang

-- the old sfa db is running on postgres 10
legacyConnString :: IO String
legacyConnString = do
  host   <- getEnv "LEGACY_DBHOST"
  port   <- getEnv "LEGACY_DBPORT"
  user   <- getEnv "LEGACY_DBUSER"
  dbname <- getEnv "LEGACY_DBNAME"
  pass   <- getEnv "LEGACY_DBPASS"
  return $ "host=" ++ host
       ++ " port=" ++ port
       ++ " user=" ++ user
       ++ " dbname=" ++ dbname
       ++ " password=" ++ pass

newtype KategorieInfo = KategorieInfo
  { infoKat :: T.Text } deriving (Show)

instance FromRow KategorieInfo where
  fromRow = KategorieInfo <$> field

newtype Unterkategorie = Unterkategorie
  { unterKat :: T.Text } deriving (Show, Eq)

instance FromRow Unterkategorie where
  fromRow = Unterkategorie <$> field

data SfaStrasse = SfaStrasse
  { lon    :: Rational
  , lat    :: Rational
  , strass :: T.Text
  , nummer :: T.Text
  } deriving (Show)

instance FromRow SfaStrasse where
  fromRow = SfaStrasse <$> field <*> field <*> field <*> field

data SfaInfo = SfaInfo
  { idInfo    :: Int
  , infoIdStr :: Int
  , infoDatum :: Day
  , info      :: Maybe T.Text
  , absender  :: Maybe T.Text
  }

instance FromRow SfaInfo where
  fromRow = SfaInfo <$> field <*> field <*> field <*> field <*> field

data Eigentuemer = Eigentuemer
  { name            :: T.Text
  , strassenr       :: T.Text
  , ort             :: T.Text
  , plz             :: T.Text
  , land            :: Maybe T.Text
  , bemerkungtuemer :: T.Text
  } deriving (Show)

instance FromRow Eigentuemer where
  fromRow = Eigentuemer <$> field <*> field <*> field <*> field <*> field <*> field

data SfaEigentum = SfaEigentum
  { ideigen       :: Int
  , idstrasse     :: Int
  , eigenDatum    :: Day
  , besitz        :: T.Text
  , bemerkungtum  :: T.Text
  , eigentuemer   :: T.Text
  , ideigentuemer :: Int
  } deriving (Show)

instance FromRow SfaEigentum where
  fromRow = SfaEigentum <$> field <*> field <*> field <*> field
                        <*> field <*> field <*> field

simpleQuery :: (FromRow a, ToRow q) => Query -> q -> IO [a]
simpleQuery queryString args = do
  conn <- connectPostgreSQL . pack =<< legacyConnString
  rows <- query conn queryString args
  close conn
  return rows

sfaStrIdToPoint :: Int -> IO (Maybe (Geometry Point))
sfaStrIdToPoint strId = do
  strNrMaybe <- qSfaStrassenrById strId
  case strNrMaybe of
    Nothing    -> do
      appendFile "problemo.csv" noStr >> pure Nothing
            where noStr = show strId ++ ", Keine Strasse mit dieser id. \n"
    Just strNr -> if (la > 47 && la < 48) && (lo > 7 && lo <8)
                  then pure $ Just geoPoint
                  else appendFile "problemo.csv" noCoord >> pure Nothing
            where noCoord = show strId ++ ", Keine Koordinaten für Strasse mit dieser id.\n"
                  lo = fromRational $ lon strNr
                  la = fromRational $ lat strNr
                  geoPoint = PointGeometry (point $ coordinate2 lo la) $ Just 4326


qEigentuemerById :: Int -> IO (Maybe Eigentuemer)
qEigentuemerById strId = listToMaybe <$> simpleQuery q [strId]
  where
    q :: Query
    q = [iTrim| SELECT
                  eigentuemer_name,
                  eigentuemer_strasse_nr,
                  eigentuemer_plz,
                  eigentuemer_ort,
                  eigentuemer_land,
                  eigentuemer_bemerkung
                FROM htab_eigentuemer
                WHERE eigentuemer_id = ?
            ;|]

qSfaStrassenrById :: Int -> IO (Maybe SfaStrasse)
qSfaStrassenrById strId = do
  result <- listToMaybe <$> simpleQuery q [strId]
  case result of
    Nothing  -> writeError' ("no strasse found with id: " ++ show strId)
                >> pure Nothing
    Just _   -> pure result
  where
    q :: Query
    q = [iTrim| SELECT strasse_lon, strasse_lat, stra, hnr
        FROM tab_strassenr
        WHERE strasse_id = ?
        ;|]

qSfaStrassenrById' :: Int -> IO (Maybe SfaStrasse)
qSfaStrassenrById' strId = listToMaybe <$> simpleQuery q [strId]
  where
    q :: Query
    q = [iTrim| SELECT strasse_lon, strasse_lat, stra, hnr
        FROM tab_strassenr
        WHERE strasse_id = ?
        ;|]

-- | We find the Kategorie name in the tab_kateigent table in a strange format
-- "xx - ${category name}" whereas xx is kinda id of the Kategorie. However, this
-- means that we have to cut the first 5 letters of the category string.
-- "unklar" is the official term used at sfa for unknown categories.
qKategorieOfInfo' :: SfaInfo -> IO T.Text
qKategorieOfInfo' e = do
  katList <- simpleQuery q [idInfo e] :: IO [KategorieInfo]
  case katList of
    []    -> do
      appendFile "problemo.csv" (show (idInfo e) ++
                        ", Keine Kategorie gefunden für Info mit dieser id \n")
      pure "unklar"
    (x:_) -> return $ T.drop 5 $ infoKat x
  where
    q :: Query
    q = [iTrim|
              SELECT
                kat_kat
              FROM
                tab_kategorien
              WHERE kat_idinfos = ?;
              |]

-- | take id of Eigentuemer and get associated Kategorie list from legacy db
-- insert found list of Kategorie to new db and return list of its ids
qKategorieOfEigentum' :: Int64 -> IO [Int64]
qKategorieOfEigentum' tuemerId = do
  katList <- simpleQuery q [tuemerId] :: IO [Unterkategorie]
  case katList of
    []    -> do
      appendFile "problemo.csv" msg
      pure []
        where msg = show tuemerId ++
                ", keine Kategorie gefunden für Eigentuemer mit dieser id \n"
    kList -> getIds (stripKat <$> kList)
  where
    stripKat :: Unterkategorie -> T.Text
    stripKat x = T.drop 5 $ unterKat x
    insertEigenkat :: T.Text -> IO Int64
    insertEigenkat = Db.insertUnique . Parzelle.Kategorie
    getIds :: [T.Text] -> IO [Int64]
    getIds = mapM insertEigenkat
    q :: Query
    q = [iTrim|
              SELECT
                kate_kateigent
              FROM
                tab_kateigent
              WHERE kate_ideigent = ?;
              |]

qKategorieOfEigentum :: SfaEigentum -> IO T.Text
qKategorieOfEigentum e = do
  katList <- simpleQuery q [ideigentuemer e] :: IO [Unterkategorie]
  case katList of
    []     -> do
      appendFile "problemo.csv" msg
      pure "Keine Kategorie"
        where msg = show (ideigentuemer e) ++
                ", keine Kategorie gefunden für Eigentuemer mit dieser id \n"
    (x:xs) -> let katSet = foldl (\a b -> -- remove duplicate Kategorien
                                    if b `notElem` a then b:a else a
                                 ) [x] xs
              in pure . T.unwords $ stripKat <$> katSet
  where
    stripKat :: Unterkategorie -> T.Text
    stripKat x = T.drop 5 $ unterKat x
    q :: Query
    q = [iTrim|
              SELECT
                kate_kateigent
              FROM
                tab_kateigent
              WHERE kate_ideigent = ?;
              |]

-- | query gebaeude at a eingang of a legacy info and return its EGID
legacyInfoToEgid :: SfaInfo -> IO (Maybe Double)
legacyInfoToEgid sfaInfo = do
  mbPoint      <- sfaStrIdToPoint $ infoIdStr sfaInfo
  -- uf, our point above is wrapped in Maybe. the function to get the gebaeude
  -- at this point returns a list wrapped in IO. this means a lot of unwrapping:
  mbGeb <- if Mb.isNothing mbPoint
           then pure Nothing
           else foldM
                (\x y -> if Mb.isNothing x
                         then listToMaybe <$> qGeoAtPoint "gebaeude" y
                         else pure Nothing
                ) Nothing mbPoint
  pure $ Gebaeude.gebaeudeEgid . unpackRecord <$> mbGeb
  where
    unpackRecord (Sql.Entity _ g) = g

-- | travel through all legacy eigentum and try to find a parzelle/its egrid to
-- associate eigentum info with it.
-- Problem: legacy eigentum info is attached to an entrance of a building aka address.
-- But: Official eigentum is attached to parcels, either Liegenschaft,
-- Bauchrecht oder Unterrecht.
--
-- First I use the geo info of an address to identify its corresponding parcel.
-- There a some problems with this approach:
-- - there are several types overlapping Parzellen
--   (Liegenschaft > Baurecht > Selbstrecht) - to which of those does the
--   Eigentum info belong?
-- - sometimes lon/lat points are slighlty unprecise and hit the wrong parzelle,
--   sometimes they are totally wrong
--
-- New: Search new db eingang with legacy address strasse and nummer as keys
-- -> the eingang found holds the egid of the corresponding gebaeude
--    this info is taken from geo admin ch portal
-- -> the gebaeude holds the egrid of the corresponding parzelle
--    + this info is also taken from geo admin ch portal
--    + we dont have to do the mapping to parzelle subtype (Liegenschat,
--    Baurecht, ...)
--    + faster than geo search algos
-- the basic address lookup is already implemented as a fallback if geo algo
-- doesnt come up with a result.
-- TODO:
-- - use address search as the primary algo
-- - improve address comparison algo (strip special characters, compare lowercase)
legacyEigentumToEgrid :: SfaEigentum -> IO (Maybe T.Text)
legacyEigentumToEgrid e = do
  -- writeError' ("Check sfa eigen with id " ++ show (ideigen e) ++ " \n")
  mbStras <- qSfaStrassenrById $ idstrasse e
  case mbStras of 
    Just str -> let stra = T.strip $ strass str
                    numm = T.strip $ nummer str
                in qEgridByStrNr stra numm
    Nothing -> pure Nothing
  -- mbPoint <- sfaStrIdToPoint $ idstrasse e
  -- mbStrNrEgrid <- case mbStras of 
  --   Just str -> let stra = T.strip $ strass str
  --                   numm = T.strip $ nummer str
  --               in writeError ("lookup: " ++ T.unpack stra ++ T.unpack numm)
  --                  >> (qEgridByStrNr stra numm)
  --   Nothing -> pure Nothing
  -- case mbStras of 
  --   Just str -> let stra = T.strip $ strass str
  --                   numm = T.strip $ nummer str
  --               in qEgridByStrNr stra numm
  --   Nothing -> pure Nothing
  -- parzLi  <- case mbPoint of
  --              Nothing -> pure []
  --              Just pnt -> map (\(Sql.Entity _ parz) -> parz)
  --                             <$> qGeoAtPoint "parzelle" pnt
  -- let iombParzEgrid | checkBaurecht && findType "Baurecht" parzLi =
  --                     pure $ listToMaybe $ Parzelle.parzelleEgrid
  --                         <$> filter (("Baurecht" ==) . Parzelle.parzelleTyp)  parzLi 
  --                   | findType "Liegenschaft" parzLi && findAllmend parzLi =
  --                     writeError ("found allmend, parzLiLen=" ++ show (length parzLi))
  --                     >> pure Nothing
  --                   | findType "Liegenschaft" parzLi =
  --                       pure $ listToMaybe (Parzelle.parzelleEgrid <$> parzLi)
  --                   | null parzLi = writeError "No Parz at Point" >> pure Nothing
  --                   | otherwise = writeError "Unidentifiable problem" >> pure Nothing
  -- mbParzEgrid <- iombParzEgrid
  -- case mbParzEgrid of
  --   Just parzEgrid -> pure $ Just parzEgrid
  --   Nothing -> case mbStrNrEgrid of
  --     Just egrid -> writeError ("strnr lookup success: egrid= " ++ T.unpack egrid ++ "\n")
  --                   >> pure  (Just egrid)
  --     Nothing -> writeError "strnr lookup failed \n" >> pure Nothing
  -- where
  --   findAllmend list = "Allmendparzelle" `elem` map Parzelle.parzelleArt list
  --   findType typ list = typ `elem` map Parzelle.parzelleTyp list
  --   checkBaurecht = T.isInfixOf "baurecht" . T.toLower $ besitz e
  --   writeError err = appendFile "problemo.csv" $ err ++ " idStr: " ++ idstr ++ "\n" 
  --   idstr = show $ idstrasse e
  --   -- getEgridByAdress :: SfaEigentum -> IO (Maybe T.Text)
  --   -- getEgridByAdress e = do
      
    
      
  -- if checkBaurecht (besitz e) && elem "Baurecht" (map Parzelle.parzelleTyp parzLi)
  -- then pure $ listToMaybe $ Parzelle.parzelleEgrid
  --                        <$> filter (("Baurecht" ==) . Parzelle.parzelleTyp)  parzLi
  -- else pure $ listToMaybe (Parzelle.parzelleEgrid <$> parzLi)

-- | convert info from legacy db to new sfa info and insert it to db
-- return id of inserted info
handleSfaInfo :: SfaInfo -> IO Int64
handleSfaInfo sfaInfo = do
  mbEgid  <- legacyInfoToEgid sfaInfo
  case mbEgid of
    Nothing -> do
        appendFile "problemo.csv" (show (idInfo sfaInfo) ++
                          ", Keine Egid gefunden für Info mit dieser id \n")
        pure 0
    Just egid -> do
      newInfoId  <- Db.insertUnique $
                      Gebaeude.Info (UTCTime (infoDatum sfaInfo) 0)
                      egid
                      (fromMaybe "" (info sfaInfo)) -- need a unpacked value for
                      (absender sfaInfo)            -- uniqueness distinction
      katTextL   <- qKatOfLegacyInfo sfaInfo
      newKatIdL  <- insertKat katTextL
      mapM_ (insertKatRelation newInfoId) newKatIdL
      pure newInfoId
    where
      insertKatRelation :: Int64 -> Int64 -> IO Int64
      insertKatRelation iId katId = Db.insertUnique (Gebaeude.InfoKategorie (Sql.toSqlKey iId) (Sql.toSqlKey katId))
      qKatOfLegacyInfo :: SfaInfo -> IO [T.Text]
      qKatOfLegacyInfo i = do
        katList <- simpleQuery q [idInfo i] :: IO [KategorieInfo]
        pure $ T.drop 5 . infoKat <$> katList
        where
          q :: Query
          q = [iTrim|
                    SELECT
                      kat_kat
                    FROM
                      tab_kategorien
                    WHERE kat_idinfos = ?;
                    |]
      
      insertKat :: [T.Text] -> IO [Int64]
      insertKat = mapM (Db.insertUnique . Gebaeude.KategorieInfo)
      -- iKey k = Sql.toSqlKey k :: Sql.Key Gebaeude.Info
      -- katKey k = Sql.toSqlKey k :: Sql.Key Gebaeude.KategorieInfo
                    -- (Sql.toSqlKey katId :: Sql.Key Gebaeude.KategorieInfo)

-- | go through all info and look for building at info's address. Populate
--   rooseli db with info attach its building egid to it.
popInfo :: IO ()
popInfo = do
  sfaInfoList <- qSfaInfo
  insertedIds <- mapM handleSfaInfo sfaInfoList
  print $ show (countEntries insertedIds) ++ " Info wurde eingefügt."
  where
    qSfaInfo :: IO [SfaInfo]
    qSfaInfo = simpleQuery q ([] :: [String])
    q :: Query
    q = [iTrim|
              SELECT
                infos_id,
                infos_idstrasse,
                infos_datum,
                infos_info,
                infos_absender
              FROM tab_infos;
             ;|]

-- | same for die Eingänge.
popEigentum :: IO ()
popEigentum = do
  sfaEigenList <- qSfaEigentum
  newEigenIds <- fromList . catMaybes <$> mapM handleSfaEigentum sfaEigenList
  print $ show (countEntries $ toList newEigenIds) ++ " Eigentum wurde eingefügt."
  where
    qSfaEigentum :: IO [SfaEigentum]
    qSfaEigentum = simpleQuery q ([] :: [String])
      where
        q :: Query
        q = [iTrim|
                  SELECT
                    eigen_id,
                    eigen_idstrasse,
                    eigen_datum,
                    eigen_besitz,
                    eigen_bemerkung,
                    eigen_eigentuemer,
                    eigen_ideigentuemer
                  FROM tab_eigentum;
                 ;|]
    -- | convert legacy eigentum to new sfa eigentum and insert it to db
    -- return id of new sfa eigentum or Nothing
    handleSfaEigentum :: SfaEigentum -> IO (Maybe Int64)
    handleSfaEigentum sfaEigen = do
      mbEgrid <- legacyEigentumToEgrid sfaEigen
      mbBesId <- handleTuemer sfaEigen
      case mbBesId of
        Just besId -> do
          katIdList <- qKategorieOfEigentum' (fromIntegral $ ideigentuemer sfaEigen)
          mapM_ (insertBesKat besId) katIdList
        Nothing -> pure ()
      case mbEgrid of
          Just p -> Just <$> Db.insertUnique (Parzelle.Eigentum
                      (UTCTime (eigenDatum sfaEigen) 0)
                      p
                      (besitz sfaEigen)
                      (besKey <$> mbBesId)
                      (bemerkungtum sfaEigen))
          Nothing -> do
            writeErr $ show (ideigen sfaEigen) ++
                     ", Keine Parzelle gefunden für Eigentum mit dieser id \n"
            pure Nothing
              where writeErr :: String -> IO ()
                    writeErr = appendFile "problemo.csv"
    handleTuemer :: SfaEigentum -> IO (Maybe Int64)
    handleTuemer sfaEigen = do
      mbTuemer <- qEigentuemerById $ ideigentuemer sfaEigen
      mbBesitzId <- let besitzende :: Eigentuemer -> Parzelle.Besitzende
                        besitzende e = Parzelle.Besitzende (name e)
                                                           (strassenr e)
                                                           (ort e)
                                                           (plz e)
                                                           (land e)
                                                           (bemerkungtuemer e)
                    in sequence $ Db.insertUnique . besitzende <$> mbTuemer
      pure mbBesitzId
    -- | take Besitzende id, 
    besKey k = Sql.toSqlKey k :: Sql.Key Parzelle.Besitzende
    katKey k = Sql.toSqlKey k :: Sql.Key Parzelle.Kategorie
    insertBesKat :: Int64 -> Int64 -> IO Int64
    insertBesKat besId katId = Db.insertUnique (Parzelle.BesitzendeKategorie (besKey besId) (katKey katId))

countEntries :: [Int64] -> Int
countEntries = length . filter (> 0) . foldl uniqueOnly []
  where uniqueOnly acc e = if   e `elem` acc
                           then acc
                           else acc ++ [e]

delDoubleEntries :: IO ()
delDoubleEntries = do
  l <- lines <$> readFile "problemo.csv"
  writeFile "problemoSet.csv" $ unlines (foldr checkSingle [] l)
  where checkSingle line newLines = if line `elem` newLines
          then newLines
          else line:newLines

qEgridByStrNr :: T.Text -> T.Text -> IO (Maybe T.Text)
qEgridByStrNr str nr = do
  valList <- Db.runActionR $
    select $ do
    (geb :& ein) <-
        from $ table @Gebaeude.Gebaeude
        `innerJoin` table @Eingang.Eingang
        `on` (\(geb :& ein) ->
          geb ^. Gebaeude.GebaeudeEgid ==. ein ^. Eingang.EingangGebaeudeEgid)
    where_ ((ein ^. Eingang.EingangStrasse ==. val str)
           &&. (ein ^. Eingang.EingangNummer ==. val nr))
    pure (geb ^. Gebaeude.GebaeudeEgrid)
  case (length valList) of
    0 -> writeError' ("no egrid found for: " ++ T.unpack str ++ T.unpack nr ++ "\n")
         >> pure Nothing
    1 -> pure $ Just $ unValue $ head valList
    _ -> writeError' ("more than 1 egrid found for: " ++ T.unpack str ++ T.unpack nr ++ "\n")
         >> (pure $ Just $ unValue $ head valList)


qGeoAtPoint :: (Sql.ToBackendKey Sql.SqlBackend record)
  => T.Text
  -> Geometry Point
  -> IO [Sql.Entity record]
qGeoAtPoint geoTable p = Db.runActionR $ Sql.rawSql
                    [i|select ?? from ${geoTable} where st_contains(geom, ?);|]
                    [Sql.PersistLiteralEscaped . writeHex $ p]


writeError' err = appendFile "problemo.csv" $ err ++ "\n" 
