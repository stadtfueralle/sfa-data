{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TypeFamilies      #-}

module ExportGebaeude ( doExport ) where

import qualified Data.Char               as Ch
import           Data.Csv                (DefaultOrdered (headerOrder),
                                          ToNamedRecord (toNamedRecord),
                                          encodeDefaultOrderedByName, header,
                                          namedRecord, (.=))
import           Data.Maybe              (fromMaybe)
import qualified Data.Text               as T
import qualified Data.Text.IO            as T
import           Data.Text.Lazy.Encoding (decodeUtf8)
import qualified Data.Text.Lazy.IO       as LT
import           Text.Printf             (printf)

import qualified Database                as Db
import qualified DatabaseParzelle        as DbP
import qualified DbRooseli               as DbR

import qualified SchemaEingang           as SchE
import qualified SchemaGebaeude          as SchG
import qualified SchemaParzelle          as SchP

import qualified EsriGlue                as Esri
import qualified PostgisHaskellBinding   as PHB

data Gebaeude = Gebaeude
  { gebaeude :: SchG.Gebaeude
  , liegen   :: Maybe SchP.Parzelle
  , baurec   :: Maybe SchP.Parzelle
  , lieEig  :: Maybe ( SchP.Eigentum
                     , Maybe SchP.Besitzende
                     , [SchP.Kategorie]
                     )
  , bauEig  :: Maybe ( SchP.Eigentum
                     , Maybe SchP.Besitzende
                     , [SchP.Kategorie]
                     )
  , eingang  :: [SchE.Eingang]
  , quartier :: String
  }

instance DefaultOrdered Gebaeude where
  headerOrder _ = header [ "Lon"
                         , "Lat"
                         , "Datum"
                         , "Quelle"
                         , "Eigentum"
                         , "Adr Eigent"
                         , "Lan Eigent"
                         , "Ort Eigent"
                         , "Eigent Bau"
                         , "Unterkateg"
                         , "admch EGRID"
                         , "lie EGRID"
                         , "bau EGRID"
                         , "Area Par"
                         , "EGID"
                         , "Anzahl Whg"
                         , "Baujahr"
                         , "Anz Gescho"
                         , "Geb Area"
                         , "Geb Katego"
                         , "Anz Eingan"
                         , "Eingang"
                         , "In eig Lie"
                         ]

instance ToNamedRecord Gebaeude where
  toNamedRecord Gebaeude{..} =
    let (mbLieEig, mbLieBes, mbLieKat) =
          case lieEig of
            Just (eig, Nothing, kate)  -> (Just eig, Nothing, Just kate)
            Just (eig, Just bes, kate) -> (Just eig, Just bes, Just kate)
            Nothing                    -> (Nothing, Nothing, Nothing)
    in namedRecord
    [ "Lon"       .= case eingang of
        []    -> 0
        (e:_) -> (\(PHB.GeoPoint p) -> head (Esri.pointToList p)) $ SchE.eingangGeom e
    , "Lat"       .= case eingang of
        []    -> 0
        (e:_) -> (\(PHB.GeoPoint p) -> Esri.pointToList p !! 1) $ SchE.eingangGeom e
    , "Datum"      .= (show . SchP.eigentumDatum <$> mbLieEig)
    , "Quelle"     .= (SchP.eigentumBemerkung <$> mbLieEig)
    , "Eigentum"   .= (SchP.besitzendeName <$> mbLieBes)
    , "Adr Eigent" .= (SchP.besitzendeStrasse_nr <$> mbLieBes)
    , "Lan Eigent" .= fromMaybe "" (mbLieBes >>= SchP.besitzendeLand)
    , "Ort Eigent" .= (SchP.besitzendeOrt <$> mbLieBes)
    , "Eigent Bau" .= ((\(_, b, _) -> maybe "" SchP.besitzendeName b)
                        <$> bauEig)
    , "Unterkateg" .= (SchP.kategorieEigentumKategorie <$> mbLieKat)
    , "admch EGRID".= SchG.gebaeudeEgrid gebaeude
    , "lie EGRID"  .= maybe "" SchP.parzelleEgrid liegen
    , "bau EGRID"  .= maybe "" SchP.parzelleEgrid baurec
    , "Area Par"   .= maybe 0 SchP.parzelleArea liegen
    , "EGID"       .= SchG.gebaeudeEgid gebaeude
    , "Anzahl Whg" .= fromMaybe (-1) (SchG.gebaeudeAnzahlwohnung gebaeude)
    , "Baujahr"    .= fromMaybe (-1) (SchG.gebaeudeBaujahr gebaeude)
    -- , "Wohnvierte" .= quartier
    , "Anz Gescho" .= fromMaybe (-1) (SchG.gebaeudeAnzahlgeschosse gebaeude)
    , "Geb Area"   .= fromMaybe (-1) (SchG.gebaeudeFlaeche gebaeude)
    , "Geb Katego" .= fromMaybe "" (SchG.gebaeudeKategor gebaeude)
    , "Anz Eingan" .= length eingang
    , "Eingang"    .= stringifyEingang eingang
    , "In eig Lie" .= show (
        maybe False (("Privatperson" ==) . SchP.kategorieEigentumKategorie) mbLieKat
        &&
        case (eingang, mbLieBes) of
          (l@(_:_), Just bes) -> checkInEigenerLieg bes l
          _                   -> False)
    ]

stringifyEingang :: [SchE.Eingang] -> T.Text
stringifyEingang = foldl
  (\b a -> T.concat [T.unwords [SchE.eingangStrasse a, SchE.eingangNummer a], "; ", b])
  ""

enquireParzOfPoint :: SchG.Gebaeude -> IO [SchP.Parzelle]
enquireParzOfPoint rawG = do
  eingL <- Db.peelValues $ DbR.qEingangByGebaeude rawG
  parzL <- (Db.peelValues $ Db.qGeoAtPoint' "parzelle" (SchE.eingangGeom $ head eingL) :: IO [SchP.Parzelle])
  let pLen = length parzL
  if pLen > 2
    then printf "Found %s Parz for %s %s" (show $ length parzL)
                                          (show $ SchE.eingangStrasse (head eingL))
                                          (show $ SchE.eingangNummer (head eingL))
                                          >> pure parzL
  else if pLen == 0
    then printf "%s %s %s" ("Found 0 Parz for " :: String)
                           (SchE.eingangStrasse (head eingL))
                           (SchE.eingangNummer (head eingL)) >> pure []
  else if pLen == 2
    then T.putStrLn (T.unwords
      [ "Found 2 Parz at", SchE.eingangStrasse (head eingL)
      , SchE.eingangNummer (head eingL) , "one of Type"
      , SchP.parzelleTyp (head parzL), "and one of Type"
      , SchP.parzelleTyp (last parzL)
      ]) >> pure parzL
  else pure parzL

-- handleGebaeude' :: SchG.Gebaeude -> MaybeT IO ()
-- handleGebaeude' rawG = do
--   eingL  <- lift $ Db.peelValues $ DbR.qEingangByGebaeude rawG
--   let parz = do
--         mbEntity <- DbR.qParzelleByEgrid (SchG.gebaeudeEgrid rawG)
--         case mbEntity of
--           Just ent -> pure $ Just $ Db.peelValue ent
--           Nothing  -> pure Nothing
--   parz'   <- MaybeT parz
--   parzL  <- lift $ enquireParzOfPoint rawG
--   liegen <- if isTyp "Liegenschaft" parz
--             then lift $ pure parz
--             else case filter (isTyp "Liegenschaft") parzL of
--                   (x:_) -> lift $ pure x
--                   []    -> lift empty
--   baurec <- if isTyp "Baurecht" parz
--                   then lift $ pure $ Just parz
--                   else MaybeT (pure Nothing)
--   lieEig <- case parz' of
--           Just p  -> Just <$> qEigentum' p
--           Nothing -> MaybeT $ pure Nothing
--   bauEig <- case baurec of
--     Just b  -> Just <$> qEigentum' liegen
--     Nothing -> lift $ pure Nothing
--   pure ()
--   where isTyp t = (t ==) . SchP.parzelleTyp


handleGebaeude :: SchG.Gebaeude -> IO Gebaeude
handleGebaeude rawG = do
  eingang  <- Db.peelValues $ DbR.qEingangByGebaeude rawG
  parzL  <- enquireParzOfPoint rawG
  -- admin ch parz is strange, lets ignore it for the time being
  -- parz <- DbR.qParzelleByEgrid $ egrid rawG
  -- let liegen = case Db.peelValue <$> parz of
  --                Just p  -> if isTyp "Liegenschaft" p
  --                           then Just p
  --                           else Nothing
  --                Nothing -> case filter (isTyp "Liegenschaft") parzL of
  --                  (x:_) -> Just x
  --                  _     -> Nothing
  let liegen = case filter (isTyp "Liegenschaft") parzL of
                   (x:_) -> Just x
                   _     -> Nothing
  let baurec = case filter (isTyp "Baurecht") parzL of
                   (x:_) -> Just x
                   _     -> Nothing
  lieEig <- case liegen of
              Just li -> qEigentum li
              Nothing -> pure Nothing
  bauEig <- case baurec of
              Just ba -> qEigentum ba
              Nothing -> pure Nothing
  if null eingang
    then print ("No Eingang found egid: " ++ show (SchG.gebaeudeEgid rawG))
    else pure ()
  pure $ Gebaeude rawG liegen baurec lieEig bauEig eingang "Valhalla"
  where
    isTyp t = (t ==) . SchP.parzelleTyp


doExport :: String -> IO ()
doExport quartier = do
  gRawL <- qGebaeude $ T.pack quartier
  gL <- mapM handleGebaeude gRawL
  LT.writeFile "gebaeude.csv" $ decodeUtf8 $ encodeDefaultOrderedByName gL
  print $ "found " ++ show (length gL) ++ " Gebaeude"

qGebaeude :: T.Text -> IO [SchG.Gebaeude]
qGebaeude quartier = Db.peelValues $ Db.qGeoInRegion "gebaeude" quartier

qEigentum :: SchP.Parzelle -> IO (Maybe ( SchP.Eigentum
                                        , Maybe SchP.Besitzende
                                        , SchP.Kategorie
                                        )
                                 )
qEigentum p = do
  eigenL <- DbP.qEigentumByEgrid $ SchP.parzelleEgrid p
  case eigenL of
    []    -> pure Nothing
    (e:_) -> pure . Just $ peelTuple e
  where
    peelTuple (a,b,c) = (Db.peelValue a, Db.peelValue <$> b, Db.peelValue c)

checkInEigenerLieg :: SchP.Besitzende -> [SchE.Eingang] -> Bool
checkInEigenerLieg b eL =
  let
    bStrip = T.strip $ SchP.besitzendeStrasse_nr b
    bStr = T.takeWhile Ch.isAlpha bStrip
    bNr  = T.filter Ch.isNumber bStrip
    compareAdr eingang acc =
      let
        eStr = T.strip $ SchE.eingangStrasse eingang
        eNr  = T.filter Ch.isNumber $ SchE.eingangNummer eingang
      in acc || (bStr == eStr && bNr == eNr)
  in foldr compareAdr False eL

--O  In eigener Liegenschaft (?)
--X  Eingangsadressen pro Gebäude in einer Zelle
--X  Strasse
--X  Nr
--X  lon
--X  lat
--X  Datum
--X  Quelle
--X  Eigentümer
--X  Adresse Eigentümer
--X  Land Eigentümer
--X  PLZ Eigentümer
--X  Ort Eigentümer
--X  Unterkategorie Eigentümer
--X  EGRID
--X  Fläche Parzelle (EGRID)
--X  EGID
--X  Anzahl Wohnungen
--X  Baujahr
--X  Wohnviertel
--X  Anzahl Geschosse (GWR, neu!)
--X  Gebäudefläche (GWR, neu!)
--X  Gebäudekategorie (GWR, neu!)
--X  Anzahl Eingänge pro Gebäude
--X  Bodeneigentümer
