{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies      #-}

module CreditSchiss (doCsvCompletion) where
import qualified Data.ByteString       as BS
import           Data.Char             (isAlpha)
import           Data.List             (find)
import qualified Data.Text             as T
import           Data.Text.Encoding    (decodeUtf8, encodeUtf8)
import           Data.Text.Metrics     (levenshtein)

import qualified Database              as Db
import qualified PostgisHaskellBinding as PHB
import qualified SchemaEingang         as SchE
import qualified SchemaGebaeude        as SchG

findEingang' :: [SchE.Eingang] -> T.Text -> T.Text -> Maybe SchE.Eingang
findEingang' eList str nr =
  find recFilter eList
  where recFilter r = cmpAlpha (strDb r) str && cmpTxt (nrDb r) nr
        nrDb  = SchE.eingangNummer
        strDb = SchE.eingangStrasse
        cmpTxt s1 s2 = T.strip s1 == T.strip s2
        lvshtn s1 s2 = levenshtein (T.strip s1) (T.strip s2) < T.length s1 `div` 5
        cleanStr = T.toLower . T.filter isAlpha
        cmpAlpha s1 s2 = cleanStr s1 == cleanStr s2

-- ! convert one csv line to (egrid, anzWhg, lon, lat)
handleLine :: [SchE.Eingang] -> T.Text -> IO T.Text
handleLine eList line = do
  let columnL   = T.split (==',') line
      mbEingang =  findEingang' eList (columnL!!2) (columnL!!3)
  case mbEingang of
    Just e -> do
      geb <- Db.peelValues $ Db.qGeoAtPoint' "gebaeude" (SchE.eingangGeom e) :: IO [SchG.Gebaeude]
      if null geb
        then pure "no gebaude found at this eingang,,,,"
        else pure $ T.concat [ T.pack . show $ SchG.gebaeudeEgid (head geb), ","
                             , fromMaybeWhg (head geb), ","
                             , head $ pointToLonLat $ SchE.eingangGeom e, ","
                             , pointToLonLat (SchE.eingangGeom e) !! 1, ","
                             , T.pack . show $ SchE.eingangEgaid e, ","
                             ]
        where fromMaybeWhg g = case SchG.gebaeudeAnzahlwohnung g of
                Just n  -> T.pack $ show n
                Nothing -> "keine Anzahl angegeben"
              pointToLonLat :: PHB.Geo a -> [T.Text]
              pointToLonLat (PHB.GeoPoint p) = T.pack . show <$> PHB.pointToList p
              pointToLonLat _                = ["Why?", ""]
    Nothing -> pure "Eingang not found,,,,"


zipLines :: [T.Text] -> [T.Text] -> T.Text
zipLines _      []     = "nothing to append"
zipLines []     _      = "no string"
zipLines (x:xs) (y:ys) = T.concat [ x, ",", y, "\n", zipLines xs ys]


doCsvCompletion :: String -> IO ()
doCsvCompletion fileN = do
  txt     <- decodeUtf8 <$> BS.readFile fileN
  recList <- Db.peelValues Db.qRecordList :: IO [SchE.Eingang]
  let lineL = T.lines txt
  appendToLines <- mapM (handleLine recList) lineL
  BS.writeFile ("completed-" ++ fileN) $ encodeUtf8 $ zipLines lineL appendToLines
