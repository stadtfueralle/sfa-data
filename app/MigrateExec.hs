module Main where

import           System.Environment (getArgs)

import           DbMigrate

main :: IO ()
main = do
  args <- getArgs
  if null args || head args == "--rooseli"
  then putStrLn "migrating rooseli db" >> DbMigrate.migrate
  else putStrLn "migrate eiter rooseli or rawGeo"
