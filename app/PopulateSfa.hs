module Main (main) where

import qualified DbImportBsGeo     as BsImport
import qualified DbImportSfaLegacy as LegImport

main :: IO ()
main = BsImport.popGebaeudeByQuartier "Basel"
    >> BsImport.popParzelleByQuartier "Basel"
    >> LegImport.popInfo
    >> LegImport.popEigentum


