{ pkgs, pgUser ? "postgres", dbName, pgHost, psql }: with pkgs;
let
  vermessung = {
    officialName = "Amtliche Vermessung (DM01AVBS06D)";
    nick = "BS_DM01AVBS06D";
    featList = [
      # "Li_ProjSelbstRecht"
      # "Li_GrundstueckPos"
      "Li_SelbstRecht"
      # "Li_ProjGrundstueckPos" # nicht ganz so wahnsinning viele Pkte
      # "Li_LSNachfuehrung" #
      # "Li_Grenzpunkt" # wahnsinnig viele Pkte entlang der Lschften
      # "Li_GrenzpunktPos" # wahnsinnig viele Pkte für jede Ecke einer Parzelle
      # "Li_ProjLiegenschaft"
      "Li_Liegenschaft"
      "Ge_Gemeindegrenze" # Grenze Gemeinde Basel
      # "Ge_GEMNachfuehrung" # komisches grosses 5-Eck über ganzen Raum Basel
      # "Ge_GEBNachfuehrung" # Einzelne Gbde in Polygonen eingezäunt..
      # "Ge_HausnummerPos" # gleich wie ge_gebäudeeingang
      # "Ei_LinienelementSymbol" # gar nix in Bettingen & Rosental
      # "Ei_Linienelement" # Treppen, Fahrbahnrand, komisches Zeug
      # "Ei_UntGebaeudeText" # Unterirdische Gbde
      "Ge_Gebaeudeeingang" # egid und edid vorhanden, gut für admin api:
      # ${egid}_${edid}
      # "PL_PLZ6" # Polygone für Postleitzahlen
      # "PL_Ortschaft" # Basel als Polygon
      # "Bo_ObjektnamePos" # Position als Punkt von wenigen komischen Objekten
      # "Bo_GebaeudenummerPos" # hat Eidg. Gebäudeidentifikator (EGID) unter
      # "r1_gwr_egi":
      # "Bo_BoFlaecheSymbol" # Position von zB Wasserbecken
      "Bo_BoFlaeche" # unter anderem sind unter art_txt Gebäude.Gebäude die
      # Gebäude Polygone drin
      # "Bo_ProjBoFlaeche" # projektierte Gebäude
      # "Ba_BSWLinienelement" # langweiliges zeug wie Baulinie.Baulinie
      # "Ba_BSWLinBeschriftungPos"
    ];
  };
  gebaeude = {
    nick = "DM_Datenmarkt";
    featList = [
      "DatenmarktGebaeudeflaeche"
      # hat ein Attribut gebnrbfs, das der Egid entspricht
      #
      # "DatenmarktGebaeude" Punkt pro Gebäude statt Umriss, sonst gleich
    ];
  };
  bezirk = {
    nick = "WE_StatistischeRaumeinheiten";
    featList = [
      "Bezirk" "Block" "Wohnviertel"
      # "Blockseite" # Nur die Seitenlinien der Blocks
    ];
  };
  shp2pgsqlCmd = path: feat:
    "${psql}/bin/shp2pgsql -s 2056:4326 -d -D -I ${path}/${feat}.dbf";
  psqlImportCmd = "${psql}/bin/psql -U ${pgUser} -d ${dbName} -h ${pgHost}";
  writeCmd = fileName: featList: builtins.concatStringsSep "\n" (
    ["${pkgs.unzip}/bin/unzip bsGeoLayers/${fileName}.zip -d bsGeoLayers/ "] ++
    map (feat:
      '' ${shp2pgsqlCmd "bsGeoLayers/${fileName}" feat} | ${psqlImportCmd} ''
    ) featList
  );
in writeShellScriptBin "importBsGeoLayers" ''
    ${writeCmd vermessung.nick vermessung.featList}
    ${writeCmd gebaeude.nick gebaeude.featList}
    ${writeCmd bezirk.nick bezirk.featList}
  ''
